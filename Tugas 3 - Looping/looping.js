var judul1 = "LOOPING PERTAMA"
var judul2 = "LOOPING KEDUA"
var angka = 2
var kalimat1 = " - I love coding"
var kalimat2 = " - I will become a mobile developer"
console.log(judul1)
while (angka <= 20) {
	console.log(angka+" "+kalimat1)
	angka = angka + 2
}
console.log(judul2)
while (angka >= 2) {
	console.log(angka+" "+kalimat2)
	angka = angka - 2
}
for (var number = 1; number <= 20; number++) {
	if ((number % 2) > 0 && (number % 3) > 0){
		console.log(number+" "+"santai")
	}
	else if ((number % 2) == 0){
		console.log(number+" "+"Berkualitas")
	}
	else if ((number % 3) == 0){
		console.log(number+" "+"I love Coding")
	}
}
for (jumlah = 1; jumlah <= 4; jumlah++){
	console.log("########")
}
for (tangga = 1; tangga <= 7; tangga++){
	switch (tangga){
		case 1: {console.log("#"); break;}
		case 2: {console.log("##"); break;}
		case 3: {console.log("###"); break;}
		case 4: {console.log("####"); break;}
		case 5: {console.log("#####"); break;}
		case 6: {console.log("######"); break;}
		case 7: {console.log("#######"); break;}
	}
}
for (catur = 1; catur <= 8; catur++){
	switch (catur){
		case 1: {console.log(" # # # #"); break;}
		case 2: {console.log("# # # # "); break;}
		case 3: {console.log(" # # # #"); break;}
		case 4: {console.log("# # # # "); break;}
		case 5: {console.log(" # # # #"); break;}
		case 6: {console.log("# # # # "); break;}
		case 7: {console.log(" # # # #"); break;}
		case 8: {console.log("# # # # "); break;}
	}
}
								