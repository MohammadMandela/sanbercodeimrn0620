function DescendingTen(num){
	if (num == undefined){
		return -1
	}
	var deret = [num]
	for (i = (num - 1); i > (num - 10); i--){
			deret.push(i)
	}
	var kalimat = String(deret) 
	return kalimat
}
function AscendingTen(num1){
	if (num1 == undefined){
		return -1
	}
	var deret1 = [num1]
	var a = num1
	while (a < (num1 + 9)){
			a ++
			deret1.push(a)
	}
	var kalimat1 = String(deret1) 
	return kalimat1
}
function ConditionalAscDesc(reference, check){
	var deret2 = [reference]
	if ((reference == undefined) || (check == undefined)){
		return -1
	}
	else if ((check % 2 != 0)){
		var b = reference
		while (b < (reference + 9)){
			b ++
			deret2.push(b)
		}
	}
	else if ((check % 2 == 0)){
		for (b = (reference - 1); b > (reference - 10); b--){
			deret2.push(b)
		}
	}
		var kalimat2 = String(deret2) 
		return kalimat2
}
function ularTangga(){
	console.log(ConditionalAscDesc(100,2))
	console.log(ConditionalAscDesc(81,1))
	console.log(ConditionalAscDesc(80,2))
	console.log(ConditionalAscDesc(61,1))
	console.log(ConditionalAscDesc(60,2))
	console.log(ConditionalAscDesc(41,1))
	console.log(ConditionalAscDesc(40,2))
	console.log(ConditionalAscDesc(21,1))
	console.log(ConditionalAscDesc(20,2))
	return ConditionalAscDesc(1,1)
}
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1
console.log(ularTangga())