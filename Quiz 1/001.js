function bandingkan(num1, num2){
	if ((num1<0) || (num2<0) || (num1==num2)){
		return -1
	}
	else if((num1 == undefined) && num2 == undefined){
		return -1
	}
	else if((num1 > num2) || num2 == undefined){
		return num1
	}
	else if((num2 > num1) || num1 == undefined){
		return num2
	}
}
function balikString(kalimat){
	var kalimatbaru = ""
	for (var i = kalimat.length - 1; i >= 0; i--){
		kalimatbaru = kalimatbaru + kalimat[i]
	}
	return kalimatbaru
}
function palindrome(sentence){
	var sentencebaru = ""
	for (var i = sentence.length - 1; i >= 0; i--){
		sentencebaru = sentencebaru + sentence[i]
	}
	return sentence == sentencebaru
}
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false