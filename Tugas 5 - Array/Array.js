function range(startNum, finishNum){
	var number = [startNum, finishNum]
	if (startNum < finishNum){
	while(startNum < finishNum - 1){
		startNum++
		number.push(startNum)
		number.sort(function(value1, value2){return value1-value2})
	}
	}	
	else if (startNum > finishNum){
	while (startNum > finishNum + 1){
		startNum--
		number.push(startNum)
		number.sort(function(value1, value2){return value2-value1})
	}
	}
	else {
		number = -1
	}
	return number
	}
console.log(range(1,10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54,50))
console.log(range())
console.log(" ")
function rangeWithStep(startNum, finishNum, step){
	var deret = [startNum]
	if (startNum <= finishNum){
		while(startNum + step < finishNum){
			startNum += step
			deret.push(startNum)
			deret.sort(function(value1, value2){return value1-value2})
		}
	}
	else if (startNum > finishNum){
		while (startNum - step >= finishNum){
			startNum -= step
			deret.push(startNum)
			deret.sort(function(value1, value2){return value2-value1})
		}
	}
	return deret
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log(" ")
function sum(startNum, finishNum, step){
	var jumlah = startNum
	var series = [startNum]
	if (typeof step === "undefined"){
		step = 1
	}
	if (typeof startNum === "undefined" && typeof finishNum === "undefined"){
		jumlah = 0
	}
	if (startNum <= finishNum){
		while(startNum + step <= finishNum){
			startNum += step
			jumlah += startNum
		}
	}
	else if (startNum > finishNum){
		while (startNum - step >= finishNum){
			startNum -= step
			jumlah += startNum
		}
	}
	return jumlah
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log(" ")
function dataHandling(){
	var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 
	console.log("Nomor ID"+":"+" "+(input[0][0]))
	console.log("Nama"+":"+" "+(input[0][1]))
	console.log("TTL"+":"+" "+(input[0][2])+" "+(input[0][3]))
	console.log("Hobi"+":"+" "+(input[0][4]))
	console.log(" ")
	console.log("Nomor ID"+":"+" "+(input[1][0]))
	console.log("Nama"+":"+" "+(input[1][1]))
	console.log("TTL"+":"+" "+(input[1][2])+" "+(input[1][3]))
	console.log("Hobi"+":"+" "+(input[1][4]))
	console.log(" ")
	console.log("Nomor ID"+":"+" "+(input[2][0]))
	console.log("Nama"+":"+" "+(input[2][1]))
	console.log("TTL"+":"+" "+(input[2][2])+" "+(input[2][3]))
	console.log("Hobi"+":"+" "+(input[2][4]))
	console.log(" ")
	console.log("Nomor ID"+":"+" "+(input[3][0]))
	console.log("Nama"+":"+" "+(input[3][1]))
	console.log("TTL"+":"+" "+(input[3][2])+" "+(input[3][3]))
	console.log("Hobi"+":"+" "+(input[3][4]))
}
dataHandling()
console.log(" ")
function balikKata(kalimat){
	var kalimatbaru = ""
	for (var i = kalimat.length - 1; i >= 0; i--){
		kalimatbaru = kalimatbaru + kalimat[i]
	}
	return kalimatbaru
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log(" ")
function dataHandling2(){
	var data = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
	data.splice(1,3,"Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro")
	console.log(data)
	var tanggal = data.slice(3,4)
	var bulan = tanggal + "".split()
	var namabulan = bulan.split("/")
	switch (namabulan[1]){
		case "05": {console.log("Mei"); break;}
	}
	namabulan.sort(function(value1, value2){return value2-value1})
	console.log(namabulan)
	var namabulan = bulan.split("/")
	var namabulan1 = namabulan.join("-")
	var nama = data.slice(1,2)
	console.log(namabulan1)
	var nama1 = nama + "".split()
	var nama2 = nama1.substring(0,15)
	return nama2
}
console.log(dataHandling2())
	

